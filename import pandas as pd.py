import pandas as pd 

import xml.etree.ElementTree as etree

tree = etree.parse("employee.xml")

root = tree.getroot()

columns = ["name", "email", "age"]

datatframe = pd.DataFrame(columns = columns)

for node in root: 

    name = node.attrib.get("name")

    mail = node.find("email").text if node is not None else None

    age = node.find("age").text if node is not None else None

    datatframe = datatframe.append(pd.Series([name, mail, age], index = columns), ignore_index = True)