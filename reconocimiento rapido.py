import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image

faceClassif = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
image=cv2.imread('MAURICIO1.jpg')

image = cv2.resize(image,(0,0),image,.4,.4)
gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)#//CAMBIAR DE BGR A RGB
plt.imshow(gray)
plt.show()

faces = faceClassif.detectMultiScale(gray,
	scaleFactor=1.1,
	minNeighbors=5,
	minSize=(30,30),
	maxSize=(1200,1200))

for (x,y,w,h) in faces:
	cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),2)

cv2.imshow('image',image)
cv2.waitKey(0)
cv2.destroyAllWindows()